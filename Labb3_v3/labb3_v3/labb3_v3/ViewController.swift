//
//  ViewController.swift
//  labb3_v3
//
//  Created by Fredrik Bredenius on 2017-11-12.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Foundation
import AVFoundation

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    private let locationManager = LocationManager.shared
    private var seconds = 0
    private var timer: Timer?
    private var distance = Measurement(value:0, unit: UnitLength.meters)
    private var locationList:[CLLocation] = []
    private var tempValue: Double = 1000
    
    var soundEffect: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        seconds = 0
        updateDisplay()
        map.delegate = self
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0 , repeats: true){_ in
            self.eachSecond()
        }
        startLocationUpdates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func playSound(){
        let path = Bundle.main.path(forResource: "Home_Run", ofType: "mp3")!
        let url = URL(fileURLWithPath: path)
        
        do{
            soundEffect = try AVAudioPlayer(contentsOf: url)
            soundEffect?.play()
        }catch{
            print("Where is the file?")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        locationManager.stopUpdatingLocation()
    }

    func eachSecond() {
        seconds += 1
        updateDisplay()
    }
    
    private func updateDisplay(){
        timeLabel.text = FormatDisplay.time(seconds)
        distanceLabel.text = String(Int(distance.value)) +  "m"
        
        if(distance.value / tempValue) > 1{
            tempValue += 1000
            playSound()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                distance = distance + Measurement(value: delta, unit: UnitLength.meters)
                let loc = [lastLocation.coordinate, newLocation.coordinate]
                map.add(MKPolyline(coordinates: loc, count: 2))
                let region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 500, 500)
                map.setRegion(region, animated: false)
            }
            
            locationList.append(newLocation)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = .black
        renderer.lineWidth = 3
        return renderer
    }
    
    
    private func startLocationUpdates() {
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.distanceFilter = 10
        locationManager.startUpdatingLocation()
    }
}

