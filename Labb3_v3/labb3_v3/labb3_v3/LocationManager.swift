//
//  LocationManager.swift
//  labb3_v3
//
//  Created by Fredrik Bredenius on 2017-11-12.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import Foundation

import CoreLocation

class LocationManager {
    static let shared = CLLocationManager()
    
    private init() { }
}
